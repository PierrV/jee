/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.ctrl;


import web.lsi.model.ActionsBD;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static lsi.m1.utils.Constantes.ADRESSE;
import static lsi.m1.utils.Constantes.EMAIL;
import static lsi.m1.utils.Constantes.CODEPOSTAL;
import static lsi.m1.utils.Constantes.DETAIL_ID;
import static lsi.m1.utils.Constantes.DETAIL_NAME;
import static lsi.m1.utils.Constantes.ERROR_COLOR;
import static lsi.m1.utils.Constantes.ERR_ADD;
import static lsi.m1.utils.Constantes.ERR_CONNEXION_INFO_MISSING;
import static lsi.m1.utils.Constantes.ERR_CONNEXION_KO;
import static lsi.m1.utils.Constantes.ERR_SUPP;
import static lsi.m1.utils.Constantes.FRM_LOGIN;
import static lsi.m1.utils.Constantes.FRM_MDP;
import static lsi.m1.utils.Constantes.ID;
import static lsi.m1.utils.Constantes.JSP_AJOUTEREMPLOYE;
import static lsi.m1.utils.Constantes.JSP_DETAILSEMPLOYE;
import static lsi.m1.utils.Constantes.JSP_GESTIONEMPLOYEE;
import static lsi.m1.utils.Constantes.JSP_HOMECONNECTION;
import static lsi.m1.utils.Constantes.NAME;
import static lsi.m1.utils.Constantes.NO_SUCCESS_COLOR;
import static lsi.m1.utils.Constantes.NO_SUCCESS_SUPP;
import static lsi.m1.utils.Constantes.SUCCESS_COLOR;
import static lsi.m1.utils.Constantes.SUCCESS_SUPP;
import static lsi.m1.utils.Constantes.SURNAME;
import static lsi.m1.utils.Constantes.TELDOM;
import static lsi.m1.utils.Constantes.TELPORT;
import static lsi.m1.utils.Constantes.TELPRO;
import static lsi.m1.utils.Constantes.*;
import web.lsi.EmployeBean;
import web.lsi.Utilisateur;

/**
 *
 * @author willy
 */
public class Controleur extends HttpServlet {

    Utilisateur userInput;
    HttpSession session;
    ActionsBD actionsBD;
    EmployeBean employe; 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        session = request.getSession();
        if( (session.getAttribute("userBean") == null) ){
            userInput = new Utilisateur();      

            userInput.setLogin(request.getParameter(FRM_LOGIN));
            userInput.setMdp(request.getParameter(FRM_MDP));

            session.setAttribute("userBean", userInput);    
       }
        actionsBD = new ActionsBD();

        if (actionsBD.verifInfosConnexion(userInput)) {            
            session.setAttribute("listeEmplKey", actionsBD.getEmployeBean());          
            isEmptyTable(request, actionsBD);
            String action = (String)request.getParameter("action");            
            if(action == null) action = "List"; 
                switch (action){
                    case "List":
                        isEmptyTable(request, actionsBD);
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;
                    case "Ajouter":                         
                        request.getRequestDispatcher(JSP_AJOUTEREMPLOYE).forward(request, response);
                        break;
                    case "Valider":                                            
                        employe = addEmployer(request);
                        if ( (employe.getNom().equals("")) || (employe.getPrenom().equals("")) ||(employe.getTelDom().equals("")) ||(employe.getTelPort().equals("")) ||
                        (employe.getTelPro().equals("")) ||(employe.getAdresse().equals("")) ||(employe.getCodePostal().equals("")) ||(employe.getVille().equals("")) ||
                        (employe.getEmail().equals("")) ){
                            request.setAttribute("errKeyList", ERR_ADD);
                            request.setAttribute("color", ERROR_COLOR);
                            request.getRequestDispatcher(JSP_AJOUTEREMPLOYE).forward(request, response);                            
                        } else {
                            if ((actionsBD.addEmploye(employe)) != 0 ){                                                              
                                session.setAttribute("listeEmplKey", actionsBD.getEmployeBean());                                
                                request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                                
                            } else {
                                request.setAttribute("errKeyList", ERR_ADD);
                                request.setAttribute("color", ERROR_COLOR);    
                                request.getRequestDispatcher(JSP_AJOUTEREMPLOYE).forward(request, response);
                            }
                        }      
                        
                                                    
                        break;                        
                    case "Voir liste": 
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;
                    default:
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;
                    case "Supprimer":                                      
                                      if (request.getParameter(ID) != null){
                                        EmployeBean employee = new EmployeBean();
                                        employee.setId(request.getParameter(ID));
                                        if (actionsBD.deleteEmploye(employee) != 0){
                                            request.setAttribute("listeEmplKey", actionsBD.getEmployeBean());    
                                            request.setAttribute("errKeyList", SUCCESS_SUPP);
                                            request.setAttribute("color", SUCCESS_COLOR);
                                        } else {
                                            request.setAttribute("errKeyList", NO_SUCCESS_SUPP);
                                            request.setAttribute("color", NO_SUCCESS_COLOR);
                                        }
                                      } else   
                                      {
                                        request.setAttribute("errKeyList", ERR_SUPP);
                                        request.setAttribute("color", ERROR_COLOR);
                                      }      
                                      isEmptyTable(request, actionsBD);
                                      request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                                      
                        break;
                    case "Details":                         
                        if (actionsBD.getEmployeBean(request.getParameter(ID)) != null) {
                           seeDetail(request, actionsBD); 
                        }else{
                            request.setAttribute("errKeyList", ERR_DETAIL);
                            request.setAttribute("color", ERROR_COLOR);
                            request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                            break;
                        }                        
                        request.getRequestDispatcher(JSP_DETAILSEMPLOYE).forward(request, response);
                        break;
                    case "Modifier":
                        employe = updateEmploye(request);
                        if ( (employe.getNom().equals("")) || (employe.getPrenom().equals("")) ||(employe.getTelDom().equals("")) ||(employe.getTelPort().equals("")) ||
                                (employe.getTelPro().equals("")) ||(employe.getAdresse().equals("")) ||(employe.getCodePostal().equals("")) ||(employe.getVille().equals("")) ||
                                (employe.getEmail().equals("")) ){
                            request.setAttribute("errKeyList", ERR_ADD);
                            request.setAttribute("color", ERROR_COLOR);
                        } else {
                            if ((actionsBD.updateEmploye(employe)) != 0 ){
                                session.setAttribute("listeEmplKey", actionsBD.getEmployeBean());
                                request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                            } else {
                                request.setAttribute("errKeyList", ERR_ADD);
                                request.setAttribute("color", ERROR_COLOR);
                            }
                        }
                        setEdit(actionsBD, request);
                        request.getRequestDispatcher(JSP_DETAILSEMPLOYE).forward(request, response);
                        break;
                    case "Deconnexion":
                        request.getRequestDispatcher(JSP_AUREVOIR).forward(request, response);  
                        this.session.invalidate();
                        break;
                }
            
            //request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
        } else {
            if ((userInput.getLogin().equals("")) || (userInput.getMdp().equals(""))) {
                request.setAttribute("errKey", ERR_CONNEXION_INFO_MISSING);
            } else {
                request.setAttribute("errKey", ERR_CONNEXION_KO);
            }            
            request.setAttribute("color", ERROR_COLOR);
            session.setAttribute("userBean", null);
            request.getRequestDispatcher(JSP_HOMECONNECTION).forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setEdit(ActionsBD actionsBD, HttpServletRequest request) {
        EmployeBean employer = actionsBD.getEmployeBean(request.getParameter(DETAIL_ID));
        request.setAttribute("editId", employer.getId()); 
        request.setAttribute("editNom", employer.getNom()); 
        request.setAttribute("editPrenom", employer.getPrenom()); 
        request.setAttribute("editTelDom", employer.getTelDom()); 
        request.setAttribute("editTelPort", employer.getTelPort());
        request.setAttribute("editTelPro", employer.getTelPro()); 
        request.setAttribute("editAdresse", employer.getAdresse()); 
        request.setAttribute("editVille", employer.getVille()); 
        request.setAttribute("editCodePostal", employer.getCodePostal()); 
        request.setAttribute("editEmail", employer.getEmail());    
    } 

    private void seeDetail(HttpServletRequest request, ActionsBD actionsBD) {
        EmployeBean employer = actionsBD.getEmployeBean(request.getParameter(ID));
        request.setAttribute("editId", employer.getId()); 
        request.setAttribute("editNom", employer.getNom()); 
        request.setAttribute("editPrenom", employer.getPrenom()); 
        request.setAttribute("editTelDom", employer.getTelDom()); 
        request.setAttribute("editTelPort", employer.getTelPort());
        request.setAttribute("editTelPro", employer.getTelPro()); 
        request.setAttribute("editAdresse", employer.getAdresse()); 
        request.setAttribute("editVille", employer.getVille()); 
        request.setAttribute("editCodePostal", employer.getCodePostal()); 
        request.setAttribute("editEmail", employer.getEmail()); 
    }

    private EmployeBean updateEmploye(HttpServletRequest request) {
        EmployeBean employeEdited = new EmployeBean();
        employeEdited.setId(request.getParameter(DETAIL_ID));
        employeEdited.setNom(request.getParameter(DETAIL_NAME));
        employeEdited.setPrenom(request.getParameter(DETAIL_SURNAME));
        employeEdited.setTelDom(request.getParameter(DETAIL_TELDOM));
        employeEdited.setTelPort(request.getParameter(DETAIL_TELPORT));
        employeEdited.setTelPro(request.getParameter(DETAIL_TELPRO));
        employeEdited.setAdresse(request.getParameter(DETAIL_ADRESSE));
        employeEdited.setCodePostal(request.getParameter(DETAIL_CODEPOSTAL));
        employeEdited.setVille(request.getParameter(DETAIL_VILLE));
        employeEdited.setEmail(request.getParameter(DETAIL_EMAIL));
        return employeEdited;
    }

    private EmployeBean addEmployer(HttpServletRequest request) {
        EmployeBean employe = new EmployeBean();
        employe.setNom(request.getParameter(NAME));
        employe.setPrenom(request.getParameter(SURNAME));
        employe.setTelDom(request.getParameter(TELDOM));
        employe.setTelPort(request.getParameter(TELPORT));
        employe.setTelPro(request.getParameter(TELPRO));
        employe.setAdresse(request.getParameter(ADRESSE));
        employe.setCodePostal(request.getParameter(CODEPOSTAL));
        employe.setVille(request.getParameter(VILLE));
        employe.setEmail(request.getParameter(EMAIL));
        return employe;
    }
    
    private void isEmptyTable(HttpServletRequest request, ActionsBD actionsBD) {
        Boolean isBoolean = actionsBD.getEmployeBean().isEmpty();
        if (isBoolean) {
            request.setAttribute("errKeyList", NO_SUCCESS_SUPP);
            request.setAttribute("color", NO_SUCCESS_COLOR);
        } 
    }
    
}