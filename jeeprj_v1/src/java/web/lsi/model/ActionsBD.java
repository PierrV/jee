/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.lsi.model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lsi.m1.utils.Constantes.MDP_BDD;
import static lsi.m1.utils.Constantes.REQUTI;
import static lsi.m1.utils.Constantes.REQEMP;
import static lsi.m1.utils.Constantes.URL;
import static lsi.m1.utils.Constantes.USER_BDD;
import web.lsi.EmployeBean;
import web.lsi.Utilisateur;

/**
 *
 * @author willy
 */
public class ActionsBD {
    
    Connection conn;
    Statement stmt;
    ResultSet rs;
    Utilisateur user;
    ArrayList<Utilisateur> listeUsers;
    ArrayList<EmployeBean> listeEmploye;

    public ActionsBD(){
        try {
            conn = DriverManager.getConnection(URL, USER_BDD, MDP_BDD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public Statement getStatement(){
        try {
            stmt = (Statement) conn.createStatement();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;       
    }
    
    public ResultSet getResultSet(String req){     
         try {
            stmt = getStatement();
            rs = stmt.executeQuery(req);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return rs;
    }
    
    
    public ArrayList getUtilisateur(){
        listeUsers = new ArrayList<>();
        try {
            rs = getResultSet(REQUTI);
            
            while (rs.next()) {
                Utilisateur userBean = new Utilisateur();
                userBean.setLogin(rs.getString("LOGIN"));
                userBean.setMdp(rs.getString("PASSWORD"));

                listeUsers.add(userBean);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listeUsers;      
    }
    
    public ArrayList getEmployeBean() {
        listeEmploye = new ArrayList<>();
        try {
            rs = getResultSet(REQEMP);
            
            while (rs.next()) {
                EmployeBean emplBean = new EmployeBean();
                emplBean.setId(rs.getString("ID"));
                emplBean.setNom(rs.getString("NOM"));
                emplBean.setPrenom(rs.getString("PRENOM"));
                emplBean.setTelDom(rs.getString("TELDOM"));
                emplBean.setTelPort(rs.getString("TELPORT"));
                emplBean.setTelPro(rs.getString("TELPRO"));
                emplBean.setAdresse(rs.getString("ADRESSE"));
                emplBean.setCodePostal(rs.getString("CODEPOSTAL"));
                emplBean.setVille(rs.getString("VILLE"));
                emplBean.setEmail(rs.getString("EMAIL"));



                listeEmploye.add(emplBean);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listeEmploye; 
    }
    
        public boolean verifInfosConnexion(Utilisateur userInput) {
            
         boolean test = false;

        listeUsers = getUtilisateur();

        for (Utilisateur userBase : listeUsers) {
            if (userBase.getLogin().equals(userInput.getLogin())
                    && userBase.getMdp().equals(userInput.getMdp())) {
                test = true;
            }
        }

        return test;
    }

    public EmployeBean getEmployeBean(String id) {
        EmployeBean emplBean = null;
        listeEmploye = new ArrayList<>();
        try {
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT * FROM EMPLOYES WHERE ID=?");                    
            preparedStatement.setString(1, id);   
            
            rs = preparedStatement.executeQuery();
                  
            while (rs.next()) {     
                emplBean = new EmployeBean();
                emplBean.setId(rs.getString("ID"));
                emplBean.setNom(rs.getString("NOM"));
                emplBean.setPrenom(rs.getString("PRENOM"));
                emplBean.setTelDom(rs.getString("TELDOM"));
                emplBean.setTelPort(rs.getString("TELPORT"));
                emplBean.setTelPro(rs.getString("TELPRO"));
                emplBean.setAdresse(rs.getString("ADRESSE"));
                emplBean.setCodePostal(rs.getString("CODEPOSTAL"));
                emplBean.setVille(rs.getString("VILLE"));
                emplBean.setEmail(rs.getString("EMAIL"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return emplBean; 
    }

    public int addEmploye(EmployeBean employe) {
        int statut = -1;
        try {
            PreparedStatement preparedStatement = 
                    conn.prepareStatement("INSERT INTO EMPLOYES "
                            + "(NOM, PRENOM, TELDOM, TELPORT, TELPRO, ADRESSE, CODEPOSTAL, VILLE, EMAIL) "
                         + "VALUES (?,?,?,?,?,?,?,?,?)");
            preparedStatement.setString(1, employe.getNom());
            preparedStatement.setString(2, employe.getPrenom());
            preparedStatement.setString(3, employe.getTelDom());
            preparedStatement.setString(4, employe.getTelPort());
            preparedStatement.setString(5, employe.getTelPro());
            preparedStatement.setString(6, employe.getAdresse());
            preparedStatement.setString(7, employe.getCodePostal());
            preparedStatement.setString(8, employe.getVille());
            preparedStatement.setString(9, employe.getEmail());
            
            statut = preparedStatement.executeUpdate(); 
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());    
        }
        return statut;
    
    }
    
        public int deleteEmploye(EmployeBean employe) {
            int verifDelete = 0;
        try { 
            PreparedStatement preparedStatement =
                    conn.prepareStatement("DELETE FROM EMPLOYES WHERE ID=?");
                    
            preparedStatement.setString(1, employe.getId());          
            verifDelete = preparedStatement.executeUpdate();    
            System.out.println("CODE RETOUR = " + verifDelete);
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());          
        }
        
         return verifDelete;    
        }

    public int updateEmploye(EmployeBean employeEdited) {
        int statut = 0;
        try {
            PreparedStatement preparedStatement = 
                    conn.prepareStatement("UPDATE EMPLOYES SET"
                            + " NOM=?, PRENOM=?, TELDOM=?, TELPORT=?, TELPRO=?, ADRESSE=?, CODEPOSTAL=?, VILLE=?, EMAIL=?"
                         + " WHERE ID=?");
            preparedStatement.setString(1, employeEdited.getNom());
            preparedStatement.setString(2, employeEdited.getPrenom());
            preparedStatement.setString(3, employeEdited.getTelDom());
            preparedStatement.setString(4, employeEdited.getTelPort());
            preparedStatement.setString(5, employeEdited.getTelPro());
            preparedStatement.setString(6, employeEdited.getAdresse());
            preparedStatement.setString(7, employeEdited.getCodePostal());
            preparedStatement.setString(8, employeEdited.getVille());
            preparedStatement.setString(9, employeEdited.getEmail());
            preparedStatement.setString(10, employeEdited.getId());
            statut = preparedStatement.executeUpdate();            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());    
        }
        return statut;
    }

}
