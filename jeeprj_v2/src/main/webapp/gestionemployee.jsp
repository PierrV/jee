<%-- 
    Document   : gestionemployee
    Created on : 17 oct. 2019, 09:44:06
    Author     : willy
--%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Bienvenue - LSI - M1</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
     
                 
        <div class="ConnectAs" align = "right">
            
            <form action="Controleur">
                <%--<c:out value="Bonjour ${requestScope.message} ! Votre session est active." /> --%>
                <c:out value="Bonjour ${userBean.login} ! Votre session est active." />
                <button type="submit" name="action" value="Deconnexion" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-off"></span> 
                </button>
            </form>                
            <%-- <input type="submit" name="action" value="Deconnexion" class="btn btn-default btn-sm"/>--%>
        </div>
        
        <c:if  test="${!empty errKeyList}">
            <span style="color:${color}">  ${ errKeyList} </span><br/>
        </c:if>
         
        <form action="Controleur">
    
        <div class="container">
        <table class="table col-md-12">
        <thead>
          <tr>
                <th>SEL</th>
                <th>NOM</th>
                <th>PRENOM</th>
                <th>TELDOM</th>
                <th>TELPORT</th>
                <th>TELPRO</th>
                <th>ADRESSE</th>
                <th>CODEPOSTAL</th>
                <th>VILLE</th>
                <th>EMAIL</th>
          </tr>
        </thead>
        <tbody>
           <c:forEach items="${listeEmplKey}" var="employe">
                <tr>
                    <td>
                        <input type="radio" value="${employe.id}" name="radio-id">
                    </td>
                    <td value="${employe.nom}" name="nom-selected">${employe.nom}</td>
                    <td value="${employe.prenom}" name="prenom-selected">${employe.prenom}</td>
                    <td value="${employe.teldom}" name="telDom-selected">${employe.teldom}</td>
                    <td value="${employe.telport}" name="telPort-selected">${employe.telport}</td>
                    <td value="${employe.telpro}" name="telPro-selected">${employe.telpro}</td>
                    <td value="${employe.adresse}" name="adresse-selected">${employe.adresse}</td>
                    <td value="${employe.codepostal}" name="codePostal-selected">${employe.codepostal}</td>
                    <td value="${employe.ville}" name="ville-selected">${employe.ville}</td>
                    <td value="${employe.email}" name="email-selected">${employe.email}</td>
                </tr>
            </c:forEach>
        </tbody>
        </table>
        
        <c:if  test="${userBean.isAdmin()}">
            <input type="submit" name="action" value="Ajouter" class="btn btn-primary"/>
            <input type="submit" name="action" value="Supprimer" class="btn btn-danger"/>
            <input type="submit" name="action" value="Details" class="btn btn-warning"/> 
        </c:if>      

        </form>
    </body>
</html>