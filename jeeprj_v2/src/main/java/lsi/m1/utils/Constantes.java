/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.utils;

/**
 *
 * @author willy
 */
public class Constantes {
    public static final String URL = "jdbc:mysql://localhost:3306/jeeprj";
    public static final String USER_BDD = "jee";
    public static final String MDP_BDD = "jee";
    public static final String ERR_CONNEXION_KO = "Echec de la connexion ! Vérifiez que votre login et/ou mot de passe et essayez à nouveau.";
    public static final String ERR_CONNEXION_INFO_MISSING = "Vous devez renseigner les deux champs";
    public static final String ERR_ADD = "Echec de l'ajout";
    public static final String ERR_SUPP = "Echec de la suppression";
    public static final String SUCCESS_SUPP = "Suppression réussie !";
    public static final String SUCCESS_COLOR = "green";
    public static final String NO_SUCCESS_COLOR = "blue";
    public static final String ERR_SELECTION = "Veuillez selectionner un employé";
    public static final String ERROR_COLOR = "red";
    public static final String FRM_LOGIN = "loginForm";
    public static final String FRM_MDP = "mdpForm";
    public static final String JSP_HOMECONNECTION = "homeconnection.jsp";
    public static final String JSP_GESTIONEMPLOYEE = "gestionemployee.jsp";
    public static final String JSP_AJOUTEREMPLOYE = "ajouteremploye.jsp";
    public static final String JSP_DETAILSEMPLOYE = "detailsemploye.jsp";
    public static final String JSP_AUREVOIR = "aurevoir.jsp";
    public static final String EDIT_USER = "radio-stacked";
        
    public static final String ID = "radio-id";
    public static final String NAME = "Nom";
    public static final String SURNAME = "Prenom";
    public static final String TELDOM = "TelDom";
    public static final String TELPORT = "TelPort";
    public static final String TELPRO = "TelPro";
    public static final String ADRESSE = "Adresse";
    public static final String CODEPOSTAL = "CodePostal";
    public static final String VILLE = "Ville";
    public static final String EMAIL = "Email";
    
    public static final String DETAIL_ID = "DetailsId";
    public static final String DETAIL_NAME = "DetailsNom";
    public static final String DETAIL_SURNAME = "DetailsPrenom";
    public static final String DETAIL_TELDOM = "DetailsTelDom";
    public static final String DETAIL_TELPORT = "DetailsTelPort";
    public static final String DETAIL_TELPRO = "DetailsTelPro";
    public static final String DETAIL_ADRESSE = "DetailsAdresse";
    public static final String DETAIL_CODEPOSTAL = "DetailsCodePostal";
    public static final String DETAIL_VILLE = "DetailsVille";
    public static final String DETAIL_EMAIL = "DetailsEmail";

}
