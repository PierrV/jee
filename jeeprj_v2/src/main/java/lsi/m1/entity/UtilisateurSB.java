/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.entity;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pierr
 */
@Stateless
public class UtilisateurSB {
    
    @PersistenceContext 
    EntityManager em;
    
    public List getUtilisateurs(){
        List l;
        Query q = em.createNamedQuery("Utilisateur.findAll");
        l = q.getResultList();
        return l;
    }
    
    public boolean getUtilisateur(Utilisateur u){
        return em.contains(u);
    }
}
