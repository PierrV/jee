/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.entity;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pierr
 */
@Stateless
public class EmployesSB {
    
    @PersistenceContext
    EntityManager em;
    
    public List getEmployes(){
        em.flush();
        List l = new ArrayList<>();
        Query q = em.createNamedQuery("Employes.findAll");
        l = q.getResultList();
        return l;
    }

    public Employes getEmploye(int id) {        
        Employes e = em.find(Employes.class, id);
        return e;
    }

    public Employes getEmploye(String id) {
        Employes e = em.find(Employes.class, Integer.valueOf(id));
        System.out.println(e);
        return e;  
    }

    public void addEmployer(Employes employe) {
        em.persist(employe);
    }

    public void deleteEmploye(Employes employe) {
        if (!em.contains(employe)) {
            employe = em.merge(employe);
            em.remove(employe);
        }
    }

    public void updateEmploye(Employes employe) {
        Employes e = em.find(Employes.class, employe.getId());
        if (e != null) {
            e = employe;
            em.merge(e);
        }
    }

}
