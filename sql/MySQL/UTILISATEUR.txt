/*On supprime les tables si elles existent */
DROP TABLE IF EXISTS UTILISATEUR;

/*Création de la table UTILISATEUR*/
CREATE TABLE UTILISATEUR (
	ID INT NOT NULL AUTO_INCREMENT,
	LOGIN VARCHAR(25) NOT NULL,
	PASSWORD VARCHAR(25) NOT NULL,
	PRIMARY KEY (ID)
);

/*Insertion de l'utilisateur admin*/
INSERT INTO UTILISATEUR(LOGIN,PASSWORD) VALUES('admin','admin');
INSERT INTO UTILISATEUR(LOGIN,PASSWORD) VALUES('empl','empl');