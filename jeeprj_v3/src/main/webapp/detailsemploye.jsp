<%-- 
    Document   : details
    Created on : 24 oct. 2019, 15:51:50
    Author     : willy
--%>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>  
        <c:if  test="${!empty errKeyList}">
            <span style="color:${color}">  ${ errKeyList} </span><br/>
        </c:if>
        <form action="Controleur">
            <div class="form-row">
              <input type="text" class="form-control" id="validationServer02" name="DetailsId" value="${employe.id}" hidden>           
              <div class="col-md-4 mb-3">
                <label for="validationServer01">Nom</label>
                <input type="text" class="form-control" id="validationServer02" name="DetailsNom" value="${employe.nom}" >
              </div>

              <div class="col-md-4 mb-3">
                <label for="validationServer02">Prénom</label>
                <input type="text" class="form-control" id="validationServer02" name="DetailsPrenom" value="${employe.prenom}" >
              </div>

              <div class="col-md-4 mb-3">
                <label for="validationServer02">Tél Dom</label>
                <input type="text" class="form-control" id="validationServer02" name="DetailsTelDom"  value="${employe.teldom}" >
              </div>

              <div class="col-md-6 mb-3">
                <label for="validationServer03">Tél Mob</label>
                <input type="text" class="form-control" id="validationServer03" name="DetailsTelPort" value="${employe.telport}" >
              </div>

              <div class="col-md-3 mb-3">
                <label for="validationServer04">Tél Pro</label>
                <input type="text" class="form-control" id="validationServer04" name="DetailsTelPro" value="${employe.telpro}" >
              </div>

              <div class="col-md-3 mb-3">
                <label for="validationServer05">Adresse</label>
                <input type="text" class="form-control" id="validationServer05" name="DetailsAdresse" value="${employe.adresse}" >
              </div>

            </div>
                      <div class="col-md-3 mb-3">
                <label for="validationServer05">Ville</label>
                <input type="text" class="form-control" id="validationServer05" name="DetailsVille" value="${employe.ville}" >
              </div>

              <div class="col-md-3 mb-3">
                <label for="validationServer05">Code Postal</label>
                <input type="text" class="form-control" id="validationServer05" name="DetailsCodePostal" value="${employe.codepostal}" >
              </div>

              <div class="col-md-3 mb-3">
                <label for="validationServer05">Adresse Mail</label>
                <input type="text" class="form-control" id="validationServer05" name="DetailsEmail" value="${employe.email}" >
              </div>

            <input type="submit" name="action" value="Modifier" class="btn btn-primary"/>
            <input type="submit" name="action" value="Voir liste" class="btn btn-primary"/>
        </form>
    </body>
</html>


