/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lsi.m1.ctrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lsi.m1.ctrl.service.EmployesFacadeREST;
import lsi.m1.ctrl.service.UtilisateurFacadeREST;
import lsi.m1.entity.Employes;
import lsi.m1.entity.Utilisateur;
import static lsi.m1.utils.Constantes.*;

/**
 *
 * @author pierr
 */
@WebServlet( "/Controleur" )
public class Controleur extends HttpServlet {
    @EJB
    private EmployesFacadeREST employesSB;
    @EJB
    private UtilisateurFacadeREST utilisateurSB;
    List<Utilisateur> listeUtilisateur;
    List<Employes> listeEmployes;
    Utilisateur userInput;
    Employes employe;
    HttpSession session;
    Boolean admin;
    Boolean online;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        session = request.getSession();
        
            if( (session.getAttribute("userBean") == null || (!userInput.getLogin().equals(request.getParameter(FRM_LOGIN)) || !userInput.getPassword().equals(request.getParameter(FRM_MDP)) ) ) ){
                if(request.getParameter(FRM_LOGIN) != null || request.getParameter(FRM_MDP) != null){
                    userInput = new Utilisateur();
                    listeUtilisateur = new ArrayList<>();
                    listeUtilisateur.addAll(utilisateurSB.findAll());
                    for (Utilisateur aUtilisateur : listeUtilisateur){
                        if (request.getParameter(FRM_LOGIN).equals(aUtilisateur.getLogin()) && request.getParameter(FRM_MDP).equals(aUtilisateur.getPassword())) 
                        {
                            userInput.setLogin(request.getParameter(FRM_LOGIN));
                            userInput.setPassword(request.getParameter(FRM_MDP));
                            online = true;
                            if(userInput.getLogin().equals("admin")){
                                admin = true;
                            } else {
                                admin = false;
                            }
                            session.setAttribute("admin", admin);
                            session.setAttribute("online", online);
                            break;
                        } else {
                            online = false;
                        }                        
                    }
                    session.setAttribute("userBean", userInput);    
                } else {
                   online = (Boolean) session.getAttribute("online");
                }
       } else {
            online = (Boolean) session.getAttribute("online");
            admin = (Boolean) session.getAttribute("admin");
        }

        if (online) {
            listeEmployes = new ArrayList<>();
            listeEmployes.addAll(employesSB.findAll());
            session.setAttribute("listeEmplKey", listeEmployes);               
            String action = (String)request.getParameter("action");            
            if(action == null) action = "List"; 
                switch (action){
                    case "List":
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;
                    case "Ajouter": 
                        if (admin) request.getRequestDispatcher(JSP_AJOUTEREMPLOYE).forward(request, response);
                        break;
                    case "Valider": 
                        employe = addEmployer(request);
                        if ( (employe.getNom().equals("")) || (employe.getPrenom().equals("")) ||(employe.getTeldom().equals("")) ||(employe.getTelport().equals("")) ||
                        (employe.getTelpro().equals("")) ||(employe.getAdresse().equals("")) ||(employe.getCodepostal().equals("")) ||(employe.getVille().equals("")) ||
                        (employe.getEmail().equals("")) ){
                            request.setAttribute("errKeyList", ERR_ADD);
                            request.setAttribute("color", ERROR_COLOR);
                        } else {
                            employesSB.create(employe);
                            if ( employesSB.find(employe.getId()).equals(employe) )
                            {
                                session.setAttribute("listeEmplKey", employesSB.findAll());
                                request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                            } else {
                                request.setAttribute("errKeyList", ERR_ADD);
                                request.setAttribute("color", ERROR_COLOR);
                            }
                        }                        
                        request.getRequestDispatcher(JSP_AJOUTEREMPLOYE).forward(request, response);     
                        break;                        
                    case "Voir liste": 
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;                    
                    case "Supprimer":                                      
                        if (request.getParameter(ID) != null){
                            employe = employesSB.find(Integer.valueOf(request.getParameter(ID)));
                            employesSB.remove(employe);
                            session.setAttribute("listeEmplKey", employesSB.findAll());
                            request.setAttribute("errKeyList", SUCCESS_SUPP);
                            request.setAttribute("color", SUCCESS_COLOR);                          
                        } else {
                          request.setAttribute("errKeyList", ERR_SELECTION);
                          request.setAttribute("color", ERROR_COLOR);
                        }
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                        break;
                    case "Details": 
                        if (request.getParameter(ID) != null){                                  
                                employe = seeDetail(request);
                                request.setAttribute("employe", employe);
                                request.getRequestDispatcher(JSP_DETAILSEMPLOYE).forward(request, response);                           
                        } else   
                        {
                          request.setAttribute("errKeyList", ERR_SELECTION);
                          request.setAttribute("color", ERROR_COLOR);
                        }
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);                        
                        break;
                    case "Deconnexion":
                        request.getRequestDispatcher(JSP_AUREVOIR).forward(request, response);  
                        this.session.invalidate();
                        break;
                    case "Modifier":
                        updateEmploye(request);
                        if ( (employe.getNom().equals("")) || (employe.getPrenom().equals("")) ||(employe.getTeldom().equals("")) ||(employe.getTelport().equals("")) ||
                                (employe.getTelpro().equals("")) ||(employe.getAdresse().equals("")) ||(employe.getCodepostal().equals("")) ||(employe.getVille().equals("")) ||
                                (employe.getEmail().equals("")) ){
                            request.setAttribute("errKeyList", ERR_ADD);
                            request.setAttribute("color", ERROR_COLOR);
                        } else {                            
                            employesSB.edit(employe);
                            if (employesSB.find(employe.getId()).equals(employe)){
                                session.setAttribute("listeEmplKey", employesSB.findAll());
                                request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                            } else {
                                request.setAttribute("errKeyList", ERR_ADD);
                                request.setAttribute("color", ERROR_COLOR);
                            }
                        }
                        request.getRequestDispatcher(JSP_DETAILSEMPLOYE).forward(request, response);
                        break;  
                    default:
                        request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
                    break;
                }
            
            //request.getRequestDispatcher(JSP_GESTIONEMPLOYEE).forward(request, response);
        } else {
            if(request.getParameter(FRM_LOGIN) == null || request.getParameter(FRM_MDP) == null){
                request.setAttribute("errKey", ERR_CONNEXION_INFO_MISSING);            
            } 
            else if ((request.getParameter(FRM_LOGIN).equals("")) || (request.getParameter(FRM_MDP).equals(""))) {
                request.setAttribute("errKey", ERR_CONNEXION_INFO_MISSING);
            } else {
                request.setAttribute("errKey", ERR_CONNEXION_KO);
            }            
            request.setAttribute("color", ERROR_COLOR);
            session.setAttribute("userBean", null);
            request.getRequestDispatcher(JSP_HOMECONNECTION).forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void setEdit(HttpServletRequest request) {
        Employes employer = employesSB.find(request.getParameter(DETAIL_ID));
        request.setAttribute("editId", employer.getId()); 
        request.setAttribute("editNom", employer.getNom()); 
        request.setAttribute("editPrenom", employer.getPrenom()); 
        request.setAttribute("editTelDom", employer.getTeldom()); 
        request.setAttribute("editTelPort", employer.getTelport());
        request.setAttribute("editTelPro", employer.getTelpro()); 
        request.setAttribute("editAdresse", employer.getAdresse()); 
        request.setAttribute("editVille", employer.getVille()); 
        request.setAttribute("editCodePostal", employer.getCodepostal()); 
        request.setAttribute("editEmail", employer.getEmail());    
    } 

    private Employes seeDetail(HttpServletRequest request) {
        Employes employer = employesSB.find(Integer.valueOf(request.getParameter(ID)));
        request.setAttribute("editId", employer.getId()); 
        request.setAttribute("editNom", employer.getNom()); 
        request.setAttribute("editPrenom", employer.getPrenom()); 
        request.setAttribute("editTelDom", employer.getTeldom()); 
        request.setAttribute("editTelPort", employer.getTelport());
        request.setAttribute("editTelPro", employer.getTelpro()); 
        request.setAttribute("editAdresse", employer.getAdresse()); 
        request.setAttribute("editVille", employer.getVille()); 
        request.setAttribute("editCodePostal", employer.getCodepostal()); 
        request.setAttribute("editEmail", employer.getEmail()); 
        return employer;
    }


    private void updateEmploye(HttpServletRequest request) {
        employe.setId(Integer.valueOf(request.getParameter(DETAIL_ID)));
        employe.setNom(request.getParameter(DETAIL_NAME));
        employe.setPrenom(request.getParameter(DETAIL_SURNAME));
        employe.setTeldom(request.getParameter(DETAIL_TELDOM));
        employe.setTelport(request.getParameter(DETAIL_TELPORT));
        employe.setTelpro(request.getParameter(DETAIL_TELPRO));
        employe.setAdresse(request.getParameter(DETAIL_ADRESSE));
        employe.setCodepostal(request.getParameter(DETAIL_CODEPOSTAL));
        employe.setVille(request.getParameter(DETAIL_VILLE));
        employe.setEmail(request.getParameter(DETAIL_EMAIL));
    }

    private Employes addEmployer(HttpServletRequest request) {
        Employes employe = new Employes();
        employe.setNom(request.getParameter(NAME));
        employe.setPrenom(request.getParameter(SURNAME));
        employe.setTeldom(request.getParameter(TELDOM));
        employe.setTelport(request.getParameter(TELPORT));
        employe.setTelpro(request.getParameter(TELPRO));
        employe.setAdresse(request.getParameter(ADRESSE));
        employe.setCodepostal(request.getParameter(CODEPOSTAL));
        employe.setVille(request.getParameter(VILLE));
        employe.setEmail(request.getParameter(EMAIL));
        return employe;
    }
    
}

